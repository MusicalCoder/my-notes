# MCBDD Provider File Upload (7-8 hrs)

* Setting to limit which referrals can have uploads
  * (1 hr) Database table to list which referrals can have uploads attached 
  * (1 hr) New Tab in Settings to set said referrals
  * (1 hr) Add file uploader to Provider Interest section in Search Details (*mostly built - just needs tested and adding 'limiting' code so it only shows when appropriate)
  * (2 hr) Add file display page for SSA/Admin to review file
  * (2 hr) Add file links (view / download) on Interested provider grid in Referral View