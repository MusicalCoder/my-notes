# Trello Productivity Options

* Each project gets its own board, and then has the following lists:
    * Objectives
    * To-Do
    * Doing
    * Completed
    * Notes

* Having a main 'Today' board
    * Braindump / Ideas / etc
    * To Do
    * Priority
    * Today
    * Waiting / Follow Up
    * Done / Completed

* ToDo board
    * Goals for the Week
    * To Do Today
    * In Progress
    * Done
    * On-Going Projects

* As a CRM [How To Use Trello As A CRM [Trello Tutorial 2019]](https://www.youtube.com/watch?v=BZSvHI6P9rs)
    * Client To Do's
    * Due Sone
    * In Progress
    * Completed
    * [Multiple] Client Project List (for each client)

* Product Mgmt [How To Use Trello To Crush Productivity](https://www.youtube.com/watch?v=B-W6DiyGKZ0&t=623s)
    * [Example Board]
