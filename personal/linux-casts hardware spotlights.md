# Linuxcasts Hardware Spotlights
### Business Cards
TWiL091 : [This Business Card runs LINUX - $3](https://www.youtube.com/watch?v=iu4AQHzhnJg&t=1994s)
> [Article](https://www.thirtythreeforty.net/posts/2019/12/my-business-card-runs-linux/)
### Keyboards
ANS159 : [CORSAIR K70 RGB MK.2 Low Profile Mechanical Gaming Keyboard](https://www.amazon.com/exec/obidos/ASIN/B07H6MWLXS/altispeed-20)
### Speakers
ANS159 : [Edifier R1280T Powered Bookshelf Speakers](https://www.amazon.com/exec/obidos/ASIN/B016P9HJIA/altispeed-20)

