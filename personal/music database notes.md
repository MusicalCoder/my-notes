# Music Database Notes

### Artist
* ArtistID
* Name
* SortName
* Type
    * Group
    * Person
    * Orchestra
    * Choir
    * Character
    * Other
* Area
* FirstReleaseDate
* LastReleaseDate
* ISNICode
* MBID

### ArtistAlias
* AliasID
* Alias
* ArtistID

### Genres
* GenreID
* Genre
* GenreDesc
* IsActive

### Albums
* AlbumID
* ArtistID
* Name
* ReleaseDate
* GenreID
* AlbumArt

### Songs
* SongID
* AlbumID
* TrackNo
* Name
* Length
* Lyric
* AcousticID

### SongAlias
* SongAliasID
* SongAlias
* SongID

### Featuring
* SongID
* ArtistID
