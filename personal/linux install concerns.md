# Linux Install Concerns
* partitioning
    * 512mb partition w/ boot flag set for UEFI
    * swap partition (or file) is a must
* Nextcloud
* syncthing
* firefox
* chrome
* tweak terminal
* fish shell
* dark theme
* telegram / slack / mattermost
* /big folder
* wireguard

## Infrastructure as code
* all changes should be done in an automater


## Ansible


## Other Sites
https://github.com/e-minguez/laptop_install - laptop install script

https://yadm.io/ - Yet Another dotFile Manager

https://direnv.net/ - unclutter your .profile

https://github.com/Nanoseb/ncTelegram - ncurses telegram client

https://github.com/X0rg/CPU-X - Free software that gathers information on CPU, motherboard and more.


    