# Linux-casts Spotlight Apps

### Audio
[Pulse Effects](https://github.com/wwmm/pulseeffects) - L4E24
### Backups
[DejaDup](https://wiki.gnome.org/Apps/DejaDup) - DL146
### Battery Life
[Slimbook Battery](https://slimbook.es/en/tutoriales/aplicaciones-slimbook/398-slimbook-battery-3-application-for-optimize-battery-of-your-laptop) - L4E19
### CAD / Drawing
[Dia](https://wiki.gnome.org/Apps/Dia) - DL146
### Color Schemes
[pywal](https://github.com/dylanaraps/pywal) - L4E28
### Cross-Platform / Virtualization
[Crossover Office](https://www.codeweavers.com/) - L4E09
[Gnome Boxes](https://wiki.gnome.org/Apps/Boxes) - L4E06
### Databases
[Momento Database](https://mementodatabase.com/index.html) - Spotlight (Rob Collins)
### Dot Files
[Yet Another Dotfile Manager (yadm)](https://yadm.io/) - MWimpress unknown source
### File Maintenance
[Rapid Photo Downloader](https://www.damonlynch.net/rapid/) - Spotlight (Wendy Hill) * DLE10
### Finance
[Akaunting](https://https://akaunting.com/) - DL151
[skrooge](https://skrooge.org/) - DL151
### Gaming
[MiniGalaxy](https://www.gamingonlinux.com/articles/minigalaxy-a-new-open-source-simple-gog-client-for-linux.15672) GOG Linux Client - GoL
### GDrive
[overGrive](https://www.thefanclub.co.za/overgrive) - L4E12
### Grub
[grub-customizer](https://launchpad.net/~danielrichter2007/+archive/ubuntu/grub-customizer) - L4E16
### HUD for Benchmarking
[MangoHUD](https://github.com/flightlessmango/MangoHud) - L4E26
### Partitioning
[Parted Magic](https://partedmagic.com/) - DL154
### Pen Drives
[Multiboot USB](http://multibootusb.org/) - DL104
### Photo Manipulation
[DarkTable](https://www.darktable.org/) - Spotlight (Wendy Hill)
### Screenshots
[Flameshot](https://flameshot.js.org/#/) - Spotlight (Emma Marshall)
### Syntax Checking
[ShellCheck](https://github.com/koalaman/shellcheck) - DL162
### Terminal User Interface
[Ly](https://github.com/cylgom/ly) - LUP342
### Terminals
[Guake](http://guake-project.org/) - L4E02
### USB Boot
[Ventoy](https://github.com/ventoy/Ventoy) - CTT

