# Firefox Tweaks

### Under Options > Privacy & Security
* Set Content Blocking to Custom
* Set Trackers to be In All Windows
* Set Cookies for Third Party Trackers
* Check Cryptominers
* Check Fingerprinters

### Extensions / Themes (these should update after syncing)
* Themes - Turn on Dark mode
* Extensions
    * uBlock Origins 
    * Container Tabs

### Config changes (about:config)
* browser.tabs.LoadBookmarksInTabs -- this forces bookmars to open in a new tab!