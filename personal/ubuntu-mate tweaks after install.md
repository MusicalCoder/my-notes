# UbuntuMate tweaks after install

* Using Mate-Tweak change layout
    * Desktop
        * Remove Home Folder icon
    * Panel
        * Change Layout to Pantheon
        * Enable pull-down terminal
        * Enable keyboard LED
    * Windows
        * So far - no changes
* Software & Updates (or Additional Drivers)
    * Other Software Tab - check Canonical Partners
* Terminal
    * update - list - upgrade - autoremove
* Move Plank
* Keyboard Shortcuts
    * Super + E for Home Folder
    * Map Window Quadrants
* Non Repo/Snap Programs to Install
  * [Master PDF](https://code-industry.net/masterpdfeditor/)