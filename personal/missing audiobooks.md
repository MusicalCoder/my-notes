# Missing Audiobooks

## Alpha and Omega (Patricia Briggs)
- [x] Cry Wolf
- [ ] Hunting Ground
- [ ] Fair Game
- [ ] Dead Heat
- [ ] Burn Bright
- [ ] Wild Sign


## Anita Blake, Vampire Hunter (Laurell K Hamilton)
- [ ] Guilty Pleasures
- [ ] The Laughing Corpse
- [ ] Circus of the Damned
- [ ] The Lunatic Cafe
- [ ] Bloody Bones
- [ ] The Killing Dance
- [ ] Burnt Offerings
- [ ] Blue Moon
- [ ] Obsidian Butterfly
- [x] Narcissus in Chains
- [x] Cerulean Sins
- [ ] Incubus Dreams
- [ ] Micah
- [ ] Danse Macabre
- [ ] The Harlequin
- [ ] Blood Noir
- [ ] Skin Trade
- [ ] Flirt
- [ ] Bullet
- [ ] Hit List
- [ ] Beauty
- [ ] Kiss the Dead
- [ ] Affliction
- [ ] Dancing
- [ ] Jason
- [ ] Dead Ice
- [ ] Wounded
- [ ] Crimson Death
- [ ] Serpentine
- [ ] Sucker Punch
- [ ] Rafael
- [ ] Smolder
- [ ] Slay
- [ ] Strange Candy


## Belgariad
### The Belgariad
- [x] Pawn Of Prophecy
- [x] Queen of Sorcery
- [x] Magician's Gambit
- [x] Castle of Wizardry
- [x] Enchanter's End Game
### The Malloreon
- [ ] Guardians of the West
- [ ] King of the Murgos
- [ ] Demon Lord of Kuranda
- [x] Sorceress of Darshiva
- [x] The Seeress of Kell
### Others
- [x] Belgarath the Sorcerer
- [x] Polgara the Sorceress
- [ ] The Rivan Codex


## Book of Swords, The (Fred Saberhagen)
- [x] The First Book of Swords
- [x] The Second Book of Swords
- [x] The Third Book of Swords
## Book of Lost Swords, The (Fred Saberhagen)
- [x] Woundhealer's Story
- [x] Sightblinder's Story
- [x] Stonecutter's Story
- [x] Farslayer's Story
- [x] Coinspinner's Story
- [x] Mindsword's Story
- [x] Wayfinder's Story
- [x] Shieldbreaker's Story


## Chronicles of Amber, The (Roger Zelazny)
- [x] Nine Princes in Amber
- [x] The Guns of Avalon
- [x] Sign of the Unicorn
- [x] The Hand of Oberon
- [x] The Courts of Chaos
- [x] The Trumps of Doom
- [x] Blood of Amber
- [x] Sign of Chaos
- [x] Knight of Shadows
- [x] Prince of Chaos

## Chronicles of The Black Company (Glen Cook)
- [x] The Black Company
- [ ] Shadows Linger
- [ ] The White Rose
- [ ] The Silver Spike
- [ ] Shadow Games
- [ ] Dreams of Steel
- [ ] Bleak Seasons
- [ ] She is the Darkness
- [ ] Water Sleeps
- [ ] Soldiers Live
- [ ] Port of Shadows


## Chronicles of Ynis Aielle, The (RA Salvatore)
- [x] Echoes of the Fourth Magic
- [x] The Witch's Daughter
- [x] Bastion of Darkness


## Cinder Spires, The (Jim Butcher)
- [x] The Aeronaut's Windlass
- [ ] Warriorborn
- [ ] The Olympian Affair


## Codex Alera (Jim Butcher)
- [ ] Furies of Calderon
- [ ] Academ's Fury
- [ ] Cursor's Fury
- [ ] Captain's Fury
- [ ] Princeps' Fury
- [ ] First Lord's Fury


## Coven, The (RA Salvatore)
- [x] Child of a Mad God
- [x] Reckoning of Fallen Gods
- [ ] Song of the Risen God


## Crimson Shadow, The (RA Salvatore)
- [x] The Sword of Bedwyr
- [x] Luthien's Gamble
- [x] The Dragon King


## Death Gate Cycle (Margaret Weis, Tracy Hickman)
- [x] Dragon Wing
- [ ] Elven Star
- [ ] Fire Sea
- [ ] Serpent Mage
- [ ] The Hand of Chaos
- [ ] Into the Labyrinth
- [ ] The Seventh Gate


## Deathstalker (Simon R Green)
- [x] Deathstalker
- [x] Deathstalker Rebellion
- [x] Deathstalker War
- [x] Deathstalker Honor
- [x] Deathstalker Destiny
- [ ] Deathstalker Legacy
- [ ] Deathstalker Return
- [ ] Deathstalker Coda

## Deed of Paksenarrion, The (Elizabeth Moon)
- [x] Sheepfarmer's Daughter
- [ ] Divded Allegiance
- [ ] Oath of Gold


## DemonWars Saga (RA Salvatore)
- [x] The Demon Awakens
- [x] The Demon Spirit
- [ ] The Demon Apostle 
- [ ] Mortalis
### Second DemonWars Saga
- [x] Ascendance
- [x] Transcendence
- [x] Immortalis
- [x] The Education of Brother Thaddius
- [x] Mather's Blood
- [x] A Song for Sadye

## Dragonlance (Margaret Weis, Tracy Hickman)
### Dragonlance Chronicles
- [x] Dragons of Autumn Twilight
- [x] Dragons of Winter Night
- [x] Dragons of Spring Dawning
- [ ] Dragons of Summer Flame

## Dreamers (David Eddings)
- [x] The Elder Gods
- [ ] The Treasured One
- [ ] Crystal Gorge
- [ ] The Younger Gods

## Dresden Files (Jim Butcher)
- [x] Storm Front
- [x] Fool Moon
- [x] Grave Peril
- [x] Summer Knight
- [x] Death Masks
- [x] Blood Rites
- [x] Dead Beat
- [x] Proven Guilty
- [x] White Night
- [x] Small Favor
- [x] Turn Coat
- [x] Changes
- [x] Side Jobs
- [x] Ghost Story
- [x] Cold Days
- [x] Shadowed Souls
- [x] Skin Game
- [x] Brief Cases
- [x] Peace Talks
- [x] Battle Ground
- [x] Working for Bigfoot


## Drizzt Du'orden (RA Salvatore)
### The Dark Elf Trilogy
- [x] Homeland
- [x] Exile
- [x] Sojourn
### The Icewind Dale Trilogy
- [x] Crystal Shard
- [x] Streams of Silver
- [x] Halflings Gem
### The Legacy of the Drow
- [x] The Legacy
- [x] Starless Night
- [x] Siege of Darkness
- [x] Passage to Dawn
### Paths of Darkness
- [x] The Silent Blade
- [x] The Spine of the World
- [x] Sea of Swords
### The Sellswords
- [x] Servent of the Shard
- [x] Promise of the Witch King
- [ ] Road of the Patriarch
### The Hunter's Blades Trilogy
- [x] The Thousand Orcs
- [x] The Lone Drow
- [x] The Two Swords
### Transitions
- [ ] The Orc King
- [ ] The Pirate King
- [ ] The Ghost King
### Neverwinter Saga
- [x] Gauntlgrym
- [x] Neverwinter
- [ ] Charon's Claw
- [ ] The Last Threshold
### The Sundering
- [x] The Companions
### Companion's Codex
- [x] Night of the Hunter
- [x] Rise of the King
- [ ] Vengeance of the Iron Dwarf
### Homecoming
- [x] Archmage
- [x] Maestro
- [x] Hero
### Generations
- [x] Timeless
- [ ] Boundless
- [ ] Relentless
### Way of the Drow
- [ ] Starlight Enclave
- [ ] Glacier's Edge
- [ ] Lolth's Warrior
### Additional
- [x] One-Eyed Jax
- [ ] Dao of Drizzt


## Dungeons & Dragons: Greyhawk (Various)
- [x] White Plume Mountain
- [x] Descent Into the Depths of the Earth
- [x] The Temple of Elemental Evil
- [x] Queen of the Demonweb Pits
- [x] Keep on the Borderlands
- [x] The Tomb of Horrors


## Farseer Trilogy (Robin Hobb)
- [x] The Farseer: Assassin's Apprentice
- [ ] Royal Assassin
- [ ] Assassin's Quest


## Fire and Thorns
- [x] The Girl of Fire and Thorns
- [ ] The Crown of Embers
- [ ] The Bitter Kingdom
- [ ] The Empire of Dreams


## Firemane Saga, The (Raymond E Feist)
- [x] King of Ashes
- [ ] Queen of Storms
- [ ] Master of Furies


## Fitz and the Fool (Robin Hobb)
- [ ] Fool's Assassin
- [ ] Fool's Quest
- [x] Assassin's Fate


## Forgotten Realms (Various)
### Cleric Quintet, The (RA Salvatore)
- [x] Canticle
- [x] In Sylvan Shadows
- [x] Night Masks
- [x] Fallen Fortress
- [x] The Chaos Curse
### Elminster
- [x] The Making of a Mage
- [x] Elminster in Myth Drannor
- [x] The Temptation of Elminster
- [x] Elminster in Hell
- [x] Elminster's Daughter
- [x] Spellstorm
- [x] Death Masks
### Knights of Myth Drannor, The (Ed Greenwood)
- [x] Swords of Eveningstar
- [ ] Swords of Dragonfire
- [x] The Sword Never Sleeps 
### Sage of Shadowdale, The
- [x] Elminster Must Die
- [x] Bury Elminster Deep
- [x] Elminster Enraged 
### Shadows of the Avatar, The (Ed Greenwood)
- [x] Shadows of Doom
- [ ] Cloak of Shadows
- [ ] All Shadows Fled
### Shandril's Saga (Ed Greenwood)
- [x] Spellfire
- [x] Crown of Fire
- [x] Hand of Fire
### Sundering, The (Book 4)
- [x] The Herald
### War of the Spider Queen (RA Salvatore)
- [x] Dissolution
- [x] Insurrection
- [x] Condemnation
- [ ] Extinction
- [ ] Annihilation
- [ ] Ressurection

## Halfblood Chronicles (Andre Norton / Mercedes Lackey)
- [x] The Elvenbane
- [ ] Elvenblood
- [ ] Elvenborn  (N/A)


## How to Train Your Dragon (Cressida Cowell)
- [x] How to Train Your Dragon
- [ ] How to Be A Pirate
- [ ] How to Speak Dragonese
- [ ] How to Cheat a Dragon's Curse
- [ ] How to Twist a Dragon's Tale
- [ ] A Hero's Guide to Deadly Dragons
- [ ] How to Ride a Dragon's Storm
- [ ] How to Break a Dragon's Heart
- [ ] How to Steal a Dragon's Sword
- [ ] How to Seize a Dragon's Jewel
- [ ] How to Betray a Dragon's Hero
- [ ] How to Fight a Dragon's Fury


## Iron Druid Chronicles (Kevin Hearne)
- [x] Besieged (short stories)
- [x] Grimoire of the Lamb
- [x] Two Tales of the Iron Druid Chronicles
- [x] Hounded
- [x] Hexed
- [x] Hammered
- [x] Tricked
- [x] Two Ravens and One Crow
- [ ] Chapel Perilous
- [x] Carniepunk: The Demon Barker of Wheat Street
- [x] Trapped
- [x] Hunted
- [x] Shattered
- [x] Three Slices
- [x] Staked
- [x] The Purloined Poodle
- [x] Oberon's Meaty Mysteries: The Squirrel on the Train
- [x] Scourged
- [x] Death & Honey
- [x] First Dangle and Other Stories

## Ink & Sigil (Kevin Hearne)
- [x] Ink & Sigil
- [x] Paper & Blood
- [ ] Candle & Crow


## Kate Daniels (Ilona Andrews)
- [x] Dark and Stormy Knights
- [x] Magic Bites
- [ ] Magic Burns
- [ ] Magic Strikes
- [x] Must Love Hellhounds
- [ ] Magic Bleeds
- [x] Hexed
- [x] Magic Slays
- [x] Gunmetal Magic
- [x] Hex Appeal
- [x] Magic Rises
- [x] Night Shift
- [x] Magic Breaks
- [ ] Magic Shifts
- [ ] Magic Stars
- [ ] Magic Binds
- [ ] Magic Triumphs
- [ ] Iron and Magic
- [x] Small Magics


## Kingkiller Chronicle (Patrick Rothfuss)
- [x] The Name of the Wind
- [x] The Wise Man's Fear
- [ ] The Slow Regard of Silent Things
- [ ] The Narrow Road Between Desires


## Lord of the Rings, The (J.R.R. Tolkein)
- [x] The Hobbit
- [x] The Fellowship of the Ring
- [x] The Two Towers
- [x] The Return of the King


## Magic ex Libris (Jim C Hines)
- [x] Libriomancer
- [ ] Codex Born
- [ ] Unbound
- [ ] Revisionary


## Magicians, The (Lev Grossman)
- [x] The Magicians
- [x] The Magician King
- [x] The Magician's Land


## Mercy Thompson (Patricia Briggs)
- [x] Moon Called
- [x] Blood Bound
- [x] Iron Kissed
- [ ] Bone Crossed
- [ ] Silver Borne
- [ ] River Marked
- [ ] Frost Burned
- [ ] Night Broken
- [x] Fire Touched
- [ ] Silence Fallen
- [ ] Storm Cursed
- [x] Smoke Bitten
- [x] Soul Taken
- [ ] Winter Lost
- [ ] Shifting Shadows


## Myth Adventures (Robert Asprin)
- [x] Another Fine Myth
- [x] Myth Conceptions
- [x] Myth Directions
- [x] Hit or Myth
- [x] Myth-ing Persons
- [x] Little Myth Marker
- [x] M.Y.T.H. Inc. Link
- [x] Myth-nomers and Im-Pervections
- [x] M.Y.T.H. Inc. in Action
- [x] Sweet Myth-tery of Live
- [x] Myth-ion Improbable
- [x] Something M.Y.T.H. Inc.
- [x] Myth-told Tales
- [x] Myth-Alliances
- [x] Myth-taken Identity
- [x] Class Dis-Mythed
- [x] Myth Gotten Gains
- [x] Myth-Chief
- [ ] Myth Fortunes
- [ ] Robert Asprin's Myth-Quoted
- [x] Robert Asprin's Myth-Fits


## Nightside (Simon R Green)
- [x] Something from the Nightside
- [x] Agents of Light and Darkness
- [x] Nightingale's Lament
- [x] Hex and the City
- [x] Paths Not Taken
- [x] Sharper than a Serpent's Tooth
- [x] Hell to Pay
- [x] The Unnatural Inquirer
- [x] Wolfsbane and Mistletoe
- [x] Just Another Judgement Day
- [x] Mean Streets
- [x] The Good, the Bad, and the Uncanny
- [x] A Hard Day's Knight
- [x] The Bride Wore Black Leather
- [x] Night Fall
- [x] Tales from the Nightside


## Percy Jackson Series (Rick Riordan)
### Percy Jackson and the Olympians
- [x] The Lightning Thief
- [x] Sea of Monsters
- [x] Titan's Curse
- [x] Battle of the Labyrinth
- [x] Last Olympian
- [x] Percy Jackson's Greek Heroes

### Heroes of Olympus Series (Percy Jackson II)
- [x] Lost Hero
- [ ] Son of Neptune
- [x] Mark of Athena
- [ ] House of Hades
- [ ] Blood of Olympus

### Kane Chronicles
- [x] Red Pyramid
- [x] Throne of Fire
- [x] Serpent's Shadow

### Magnus Chase & the Gods of Asgard
- [x] Sword of Summer
- [ ] Hammer of Thor
- [ ] Ship of the Dead
- [ ] 9 From the Nine Worlds
- [ ] The Hotel Valhalla Guide to the Norse Worlds

### Trials of Apollo
- [x] The Hidden Oracle
- [ ] The Dark Prophecy
- [ ] The Burning Maze
- [ ] The Tyrant's Tomb
- [ ] The Tower of Nero
- [ ] Camp Jupiter Classified

## Razing Hell
- [x] For The Hell of It
- [x] Hell to Pay
- [x] Hell Hath No Fury
- [x] All Hell Breaks Loose
- [x] Hell Bent


## Ranger's Apprentice (John Flanagan)
- [x] The Ruins of Gorlan
- [x] The Burning Bridge
- [ ] The Icebound Land
- [ ] The Battle for Skandia
- [ ] The Sorcerer of the North
- [ ] The Siege of Macindaw
- [ ] Erak's Ransom
- [ ] The Kings of Clonmel
- [ ] Halt's Peril
- [ ] The Emperor of Nihon-Ja
- [ ] The Royal Ranger: A New Beginning
- [ ] The Royal Ranger: The Red Fox Clan
- [ ] Duel at Araluen
- [ ] The Lost Stories


## Riftwar, The (Raymond E Feist)
### Riftwar Saga, The
- [x] Magician: Apprentice
- [x] Magician: Master
- [x] Silverthorn
- [x] A Darkness at Sethanon
### Empire Trilogy, The
- [ ] Daughter of the Empire
- [ ] Servant of the Empire
- [ ] Mistress of the Empire
### Riftwar Legacy, The
- [x] Krondor: The Betrayal
- [ ] Krondor: The Assassins
- [ ] Krondor: Tear of the Gods
- [ ] Jimmy and the Crawler
### Krondor's Sons
- [x] Prince of the Blood
- [ ] The King's Buccaneer
### Serpentwar Saga, The
- [x] Shadow of a Dark Queen
- [x] Rise of a Merchant Prince
- [x] Rage of a Demon King
- [x] Shards of a Broken Crown
### Legends of the Riftwar
- [x] Honored Enemy
- [x] Murder In LaMut
- [ ] Jimmy The Hand
### Conclave of Shadows
- [x] Talon of the Silver Hawk
- [x] King of Foxes
- [x] Exile's Return
### Darkwar Saga, The
- [x] Flight of the Nighthawks
- [x] Into a Dark Realm
- [x] Wrath of a Mad God
### Demonwar Saga, The
- [ ] Rides a Dread Legion
- [ ] At the Gates of Darkness
### Chaoswar Saga
- [ ] A Kingdom Besieged
- [ ] A Crown Imperiled
- [ ] Magician's End


## Shadowrun Legends: Secrets of Power (Robert N Charrette)
- [x] Never Deal With a Dragon
- [ ] Choose Your Enemies Carefully
- [ ] Find Your Own Truth


## Spearwielder's Tales (RA Salvatore)
- [x] The Woods Out Back
- [x] The Dragon's Dagger
- [x] Dragonslayer's Return


## Sword of Truth, The (Terry Goodkind)
- [x] Debt of Bones
- [x] Wizard's First Rule
- [x] Stone of Tears
- [x] Blood of the Fold
- [x] Temple of the Winds
- [x] Soul of the Fire
- [x] Faith of the Fallen
- [x] The Pillars of Creation
- [x] Naked Empire
- [x] Chainfire
- [x] Phantom
- [x] Confessor
- [x] The Omen Machine
- [x] The Third Kingdom
- [x] Severed Souls
- [x] Warheart

## Tales of Pell, The (Keven Hearne)
- [x] Kill the Farm Boy
- [x] No Country for Old Gnomes
- [x] The Princess Beard

