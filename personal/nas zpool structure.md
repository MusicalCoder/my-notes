# NAS ZPool Structure
### CONSERVATORY
* Chamber
    * Subfolder for each machine in the house
* Vault
    * Documents
    * Spreadsheets
    * PDFs
    * eBooks
    * Textfiles
    * other (others may even be added)
* Crypt (need to sort)

### CHRONICLE
* Archive
    * Master backups
* Library
    * All documents
* Media
    * Music
    * Movies
    * TV Shows
    * Karaoke
    * Photos
    * Images
 