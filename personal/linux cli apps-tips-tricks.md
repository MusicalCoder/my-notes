# Linux CLI Apps / Tips / Tricks

## Apps
### [Bandwhich](https://github.com/imsnif/bandwhich) [LUP336]
> Terminal bandwidth utilization tool 

### [Firefox Send CLI](https://gitlab.com/timvisee/ffsend) [LUP336]
> Send a large file without needing to send through email

### [iftop](http://www.ex-parrot.com/pdw/iftop/) [LUP336]
> display bandwidth usage on an interface

### [ncspot](https://snapcraft.io/ncspot) [UPS13E02]
> ncurses spotify music client

### [nethogs](https://github.com/raboof/nethogs) [LUP336]
> a small ‘net top’ tool.

### [s-tui](https://github.com/amanusk/s-tui) [LUP336]
> Terminal-based CPU stress and monitoring utility

### [vimtutor](https://superuser.com/questions/246487/how-to-use-vimtutor) [DL104]
> Terminal app for learning VIM keybindings


## bash shell tricks/tips
### bash commands [DL101]
:::success
* ```ctrl+c``` aborts a command in linux
* ```reset``` clears the terminal like a new window
* ```clear``` clears the terminal (removes text)
    * ```ctrl+l``` is a shortcut for clear
* ```ctrl+d``` shortcut to close terminal window
* ```history``` shows a numbered list of the commands you have previously typed. Just mouse scroll back through the list until you see the command you want and then simply type ! followed by the number in the list ie !6 or !132
    * If you have a large history – try searching – ```Ctrl + R``` to invoke “reverse-i-search.” Type a letter – like s – and you’ll get a match for the most recent command in your history that starts with s. Keep typing to narrow your match. When you hit the jackpot, press Enter to execute the suggested command. Each press of Ctrl+R will cycle through other commands that might match.
* ```Ctrl + Arrow Keys``` (left or right) – to jump between segments of a command
* ```Ctrl + K``` deletes everything after the cursor
* ```Ctrl + U``` deletes everything up to the cursor
:::

### cheat.sh [Practical IT](lbry://@jeremyleik#a/quick-tips-cheat-sh-2020-practical-it#e)

```curl https://cheat.sh/{command}``` will give you a nicer reference to using {command} then a man page

### cp switches [DL099,DL154]
:::success
Using ‘cp’ to copy files in Linux is a very convenient tool. However, knowing these switches can save you from making mistakes:
* -i : Confirm before overwriting
* -n : No overwriting
* -b : Overwriting with backup
* -r : Recursive copying
* -p : Preserve the ownership and file permissions 
* -u : Overwrite if the target file is old or doesn’t exist
:::
:::info
Example
 ```cp -b sourcedirectory targetdirectory``` will overwrite the file if it exists but creates a backup file first.
:::
### sudo !! [DL107]
:::success 
sudo !! which will add sudo to your last command in case you forgot to include sudo
:::
:::info
Example
```alias please=”sudo !!”```
:::
### sudo timeout [DL162]
:::success
You can use sudo visudo command and change the default amount of time sudo waits to re-ask for your sudo password.  
Simply find the Defaults env_reset line and add a timeout to extend (or reduce/eliminate) the reset period
:::
:::info
Example
```sudo visudo```
```Defaults env_reset timestamp_timeout=5```
for 5 minutes, 0 for always ask
:::
### terminal attitude [DL105]
:::success
Give your terminal a bad attitude
1. Type visudo
1. At the bottom of the ‘Defaults’ add a new line ‘Defaults insults’.
1. Save the file

Now when you mistype your password you get insulted.
:::
:::info
Example Ryan got:
>Harm can come to a young lad like that!
There’s nothing wrong with you that an expensive operation can’t prolong.
Where did you learn to type?
:::
### terminal columns [DL097]
:::success
Display output as a table in the terminal for easier viewing using:
> | column -t

Examples:
> ```cat /etc/passwd | column -t```
> ```ls -l | column -t```
:::
### xkill [DL090]
:::success
While Linux is very stable as a rule there are times when you need to kill an application that is working incorrectly.
:::
> One of my favorite commands to kill a frozen windows is xkill. When you type this command in a terminal you get a cross hair cursor and any window you select is forcefully terminated

## find commands
DL089 : [apropos](http://www.linfo.org/apropos.html)

## non-bash shells
DL096 : Alternate Shells

:::success

Everything in Linux is customizable including the shell. Most users probably utilize whatever shell (likely Bash) that comes with their distro by default. However, there are some powerful shell options out there to try out.
* Zsh – designed to be interactive and it incorporates many features of other Unix/GNU Linux shells such as bash, tcsh and ksh.
* Tcsh – is enhanced C shell, it can be used as aN interactive login shell as well as a shell script command processor.
* Fish – “friendly interactive shell” and was authored in 2005. It was intended to be fully interactive and user friendly.
:::

## scp [DL154]
:::success
to copy a file from your local computer to a remote LAN system using ssh
rsync for remote systems that are over the internet
:::

## snaps [DL093]
:::success
* ```snap revert``` – reverts package to previous version along with data
* ```snap find``` – “media players” will search snap store for media players
* ```snap info``` – to get information about a specific snap
* Channels
    * ```–stable, –candidate, –beta and –edge``` 
:::
>Example
> ```snap install –channel=beta vlc```

### systemctl [UPS13E04]
> Use to access various system controls.  To restart pulseaudio for example
> 
> ```systemctl --user restart pulseaudio```
