# VS Code Extentions
* Bash Debug
* Bash IDE
* Bracket Pair Colorizer
* GitHub Pull Requests
* Live Share
* Markdown All In One
* Markdown Preview Showcase
* Material Icon Theme
* Ubuntu VS Code Theme
