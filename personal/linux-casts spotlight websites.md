# Linuxcasts Spotlight Websites

### Games/Gaming
L4E06 : [Gaming On Linux](https://www.gamingonlinux.com/) 

L4E04 : [ProtonDB](https://protondb.com/) Steam Windows library

L4E22 : [CLI Games](https://linoxide.com/linux-how-to/linux-command-line-games/)
### Technology Updates
L4E05 : [Dell/Barton's Blog](https://bartongeorge.io/) Updates for Linux on Dell
### Terminal/Shell Scripting
L4E03 : [ManKier](https://mankier.com/) Better man page explanations
### Windows / Virtual Machines
L4E05 : [DistroTest](https://distrotest.net/) Virt Machines for testing distros

L4E09 : [WineHQ](https://www.winehq.org/)

