# Linux DE Tips / Tricks

### KDE Plasma
CubicleNate : [Quick Tiling in KDE Plasma on openSUSE](https://cubiclenate.com/2020/01/02/quick-tiling-in-kde-plasma-on-opensuse/)
>[Youtube version](https://www.youtube.com/watch?v=bBTei7bO3iI&feature=emb_logo)

[YT] DasGeek : [Michael's Custom KDE Setup](https://www.youtube.com/watch?v=v18Nz5SLOOw&t=8s)

[YT] JupiterBroadcasting : [Perfect Plasma Setup](https://www.youtube.com/watch?v=34F_038G5pU&t=630s)

[YT] Dark1 Linux : [My KDE Plasma Customizations](https://www.youtube.com/watch?v=OCPxVWASIGo)

[YT] Chris Titus Tech : [Personal KDE Plasma Customization](https://www.youtube.com/watch?v=nRtyFtpf5yU)

[YT] #!/bin/bash : [Personal KDE Plasma Customization](https://www.youtube.com/watch?v=uyz4-KZOzyI&list=PLYskyXrgxarYe0wWm6MqmjujmsAX2B1vr&index=4&t=0s)

[YT] Nayam Amarshe : [Personal KDE Plasma Customization](https://www.youtube.com/watch?v=VL7B6oBaTfs)

[YT] ITTwist : [Personal KDE Plasma Customization](https://www.youtube.com/watch?v=qG_KBpZ_DSQ)

[YT] Thiago Leite : [KDE Plasma macOS Theme - Manjaro](https://www.youtube.com/watch?v=kotYskfykl0)

### XFCE
[YT] DasGeek : [Modernizing XFCE for 2020](https://www.youtube.com/watch?v=TTd7XjR8h4w&list=PLYskyXrgxarYe0wWm6MqmjujmsAX2B1vr&index=3&t=0s)

### elementaryOS
[YT] Linux4Everyone : [elementaryOS Unfiltered](https://www.youtube.com/watch?v=TFtaOk-U7nw&t=1931s)